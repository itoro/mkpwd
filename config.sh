#!/bin/sh

set -e

VERSION=0.2-dev
DATE="$(date -u +%Y-%m-%d)"

INFILE="$1.in"
FILE="$1"

sed \
    -e "s/\$DATE/$DATE/" \
    -e "s/\$VERSION/$VERSION/" \
    "$INFILE" >"$FILE"
