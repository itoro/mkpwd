struct error {
  const char *const file;
  const int line;
  const std::string message;
};

#define FAIL(msg)                               \
  do {                                          \
    throw error{__FILE__, __LINE__, (msg)};     \
  } while (false)

#define FAILWITH(f)                             \
  do {                                          \
    std::ostringstream oss;                     \
    (f)(oss);                                   \
    FAIL(oss.str());                            \
  } while (false)
