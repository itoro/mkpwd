#include <cmath>
#include <cstring>

#include <iostream>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

#include "version.hpp"
#include "error.hpp"
#include "random.hpp"

#include "main.hpp"

struct complexity {
  enum kind_t {
    CHARS,
    ENTROPY
  } kind;
  int count;
};

static auto showhelp = false;
static auto showversion = false;
static std::unordered_set<char> charset;
static complexity cpl {complexity::ENTROPY, 128};
static auto device = "/dev/urandom";

static const char *const helplines[] = {
  "usage: mkpwd [options]",
  "  --help           Show this help",
  "  --version        Show version",
  "  entropy=ENTROPY  Set entropy in bits",
  "  length=LENGTH    Set password length",
  "  device=DEVICE    Set random device",
  "  charset=CHARSET  Set charset",
  "    use a lower case char to allow lower case chars",
  "    use an upper case char to allow upper case chars",
  "    use a digit to allow 10 radix digits",
  "    use x to allow 16 radix digits in lower case",
  "    use X to allow 16 radix digits in upper case",
  "    use o to allow 8 radix digits",
  "    use b to allow 2 radix digits",
  "    use a symbol to allow symbols"
};

static auto help(std::ostream &os) {
  for (const auto line : helplines) {
    os << line << '\n';
  }
}

static auto parseint(const char *arg) {
  std::istringstream iss {arg};
  int n;
  iss >> n;
  if (iss.fail() || !iss.eof()) {
    FAILWITH
      ([=](auto &oss) {
        oss << "not an integer: " << arg;
      });
  }
  return n;
}

static auto setcomplexity(complexity::kind_t k, const char *arg) {
  cpl.count = parseint(arg);
  cpl.kind = k;
}

static auto setentropy(const char *arg) {
  setcomplexity(complexity::ENTROPY, arg);
}

static auto setlength(const char *arg) {
  setcomplexity(complexity::CHARS, arg);
}

static auto allowchar(unsigned char c) {
  charset.insert(c);
}

static auto allowrange(char first, char last) {
  for (auto c = first; c <= last; c++) {
    allowchar(c);
  }
}

struct charsetpart {
  char first;
  char last;
};

static const charsetpart symbolparts[] = {
  { '!', '/' },
  { ':', '@' },
  { '[', '`' },
  { '{', '~' }
};

static auto allowdigits() {
  allowrange('0', '9');
}

static auto allowlowerhexa() {
  allowrange('0', '9');
  allowrange('a', 'f');
}

static auto allowupperhexa() {
  allowrange('0', '9');
  allowrange('A', 'F');
}

static auto allowoctal() {
  allowrange('0', '7');
}

static auto allowbinary() {
  allowrange('0', '1');
}

static auto allowupper() {
  allowrange('A', 'Z');
}

static auto allowlower() {
  allowrange('a', 'z');
}

static auto allowsymbols() {
  for (const auto &part : symbolparts) {
    allowrange(part.first, part.last);
  }
}

static auto setcharset(const char *arg) {
  charset.clear();
  for (auto i = (size_t) 0U; arg[i] != '\0'; i++) {
    const auto c = arg[i];
    if ('0' <= c && c <= '9') {
      allowdigits();
    } else if (c == 'x') {
      allowlowerhexa();
    } else if (c == 'X') {
      allowupperhexa();
    } else if (c == 'o') {
      allowoctal();
    } else if (c == 'b') {
      allowbinary();
    } else if ('A' <= c && c <= 'Z') {
      allowupper();
    } else if ('a' <= c && c <= 'z') {
      allowlower();
    } else if (0x21 <= c && c <= 0x7e) {
      allowsymbols();
    } else {
      FAILWITH
        ([=](auto &oss) {
          oss << "unknown charset char: " << c;
        });
    }
  }
}

static auto setdevice(const char *arg) {
  device = arg;
}

class option_t {
public:
  const char *const prefix;
  void (*const handler) (const char *);
};

static const option_t options[] = {
  { "entropy", &setentropy },
  { "length", &setlength },
  { "device", &setdevice },
  { "charset", &setcharset }
};

static auto isprefix(const char *arg, const char *prefix, size_t len) {
  return strncmp(arg, prefix, len) == 0 && arg[len] == '=';
}

static auto parseoptions(const char *arg) {
  for (const auto &opt : options) {
    const auto len = strlen(opt.prefix);
    if (isprefix(arg, opt.prefix, len)) {
      opt.handler(&arg[len + 1]);
      return;
    }
  }
  FAILWITH
    ([=](auto &oss) {
      oss << "unknown option: " << arg;
    });
}

static auto parsearg(const char *arg) {
  if (strcmp(arg, "--help") == 0) {
    showhelp = true;
  } else if (strcmp(arg, "--version") == 0) {
    showversion = true;
  } else {
    parseoptions(arg);
  }
}

static auto parse(char *const argv[]) {
  for (auto i = 1; argv[i] != NULL; i++) {
    parsearg(argv[i]);
  }
}

static auto setdefaultcharset() {
  allowdigits();
  allowlower();
}

static auto mkcharset() {
  std::vector<char> ret (charset.size());
  auto i = (size_t) 0U;
  for (auto c : charset) {
    ret[i++] = c;
  }
  return ret;
}

static auto computelength(int n) {
  switch (cpl.kind) {
    case complexity::CHARS:
      return cpl.count;
    case complexity::ENTROPY: {
      const auto entropy = cpl.count * log(2.0) / log(n);
      return (int) ceil(entropy);
    }
    default:
      FAIL("unknown complexity");
  }
}

static auto process(char *argv[]) {
  setdefaultcharset();
  parse(argv);
  if (showhelp) {
    help(std::cout);
  } else if (showversion) {
    std::cout << "mkpwd, version " << version << ", " << date << '\n';
  } else {
    const auto charset = mkcharset();
    const auto length = computelength(charset.size());
    if (length < 1) {
      FAIL("length must be positive");
    }
    draw(device, length, charset);
  }
  std::cout << std::flush;
  if (std::cout.fail()) {
    FAIL("IO failed");
  }
}

int main(int, char *argv[]) {
  try {
    process(argv);
  } catch (error &err) {
    std::cerr
      << err.file << ':' << err.line << ": "
      << err.message << std::endl;
    return 1;
  } catch (...) {
    std::cerr << "unknown error";
    return 2;
  }
}
