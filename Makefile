SHELL=sh

include .config.mk

CPPSTD=-std=c++14 -pedantic
CPPARGS=-Wall -Wextra -Werror -Wfatal-errors -O0
CPP=$(CPPCMD) $(CPPSTD) $(CPPARGS)
RM=rm -f
CP=cp -f
MKDIR=mkdir -p
MV=mv -f
GZIP=gzip -f

.PHONY: all clean config depend

all: bin man

clean:
	$(RM) version.hpp mkpwd mkpwd.1 mkpwd.1.gz *.o

config: version.hpp mkpwd.1

version.hpp: config.sh version.hpp.in
	./config.sh version.hpp

mkpwd.1: config.sh mkpwd.1.in
	./config.sh mkpwd.1

depend: config
	$(CPPCMD) -MM *.cpp >.depend.mk

.PHONY: bin man

bin: mkpwd

man: mkpwd.1.gz

mkpwd: random.o main.o
	$(CPP) -o mkpwd random.o main.o -lm

mkpwd.1.gz: mkpwd.1
	@$(MV) mkpwd.1 mkpwd.1.bak
	@$(CP) mkpwd.1.bak mkpwd.1
	@$(RM) mkpwd.1.gz
	$(GZIP) mkpwd.1
	@$(MV) mkpwd.1.bak mkpwd.1

.PHONY: install install-man install-bin

install: install-bin install-man

BINDIR=$(DESTDIR)$(PREFIX)/bin

MANDIR=$(DESTDIR)$(PREFIX)/man/man1

install-bin: $(BINDIR)
	$(CP) mkpwd $(BINDIR)/mkpwd

install-man: $(MANDIR)
	$(CP) mkpwd.1.gz $(MANDIR)/mkpwd.1.gz

$(BINDIR):
	$(MKDIR) $(BINDIR)

$(MANDIR):
	$(MKDIR) $(MANDIR)

.SUFFIXES:
.SUFFIXES: .cpp .o

.cpp.o:
	$(CPP) -c -o $@ $<

include .depend.mk
