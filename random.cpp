#include <algorithm>
#include <string>
#include <vector>

#include <fcntl.h>
#include <unistd.h>

#include "error.hpp"

#include "random.hpp"

static auto s_open(const char *dev, int flags) {
  const auto f = open(dev, flags);
  if (f == -1) {
    FAIL("open failed");
  }
  return f;
}

template<class T>
static auto s_read(int fd) {
  T data;
  const auto sz = sizeof(data);
  const auto r = read(fd, &data, sz);
  if (r == -1 || (size_t) r != sz) {
    FAIL("read failed");
  }
  return data;
}

template<class T>
static auto s_write(int fd, const T &data) {
  const auto sz = sizeof(data);
  const auto w = write(fd, &data, sz);
  if (w == -1 || (size_t) w != sz) {
    FAIL("write failed");
  }
}

class file {
public:
  file() : _fd(invalid) {}
  file(const char *dev, int flags) : file() {
    this->take(s_open(dev, flags));
  }
  file(file &&other) : file() {
    *this = std::move(other);
  }
  ~file() {
    this->reset();
  }
  file &operator= (file &&other) {
    this->take(other.release());
    return *this;
  }
  explicit operator bool() const {
    return this->_fd != invalid;
  }
  int get() const {
    return this->_fd;
  }
  file(file &) = delete;
  file &operator= (file &) = delete;
private:
  static constexpr int invalid = -1;
  int _fd;
  void reset() {
    if (*this) {
      close(this->_fd);
      this->_fd = invalid;
    }
  }
  int release() {
    const auto fd = this->_fd;
    this->_fd = invalid;
    return fd;
  }
  void take(int fd) {
    this->reset();
    this->_fd = fd;
  }
};

class mem {
public:
  ~mem() {
    auto beg = (char *) this;
    auto end = beg + sizeof(*this);
    std::fill(beg, end, '\0');
  }
  uint8_t left;
  uint8_t num;
  uint8_t dbit;
  uint8_t cur;
  uint8_t max;
};

static auto openrnd(mem &st, const char *dev) {
  file f {dev, O_RDONLY};
  st.left = 0U;
  return f;
}

static auto fillrnd(const file &fd, mem &st) {
  if (st.left == 0U) {
    st.num = s_read<decltype(st.num)>(fd.get());
    st.left += 8U * sizeof(st.num);
  }
}

static auto drawrnd(const file &fd, mem &st) {
  fillrnd(fd, st);
  st.dbit = st.num % 2U;
  st.num /= 2U;
  st.left--;
}

static auto drawany(const file &fd, mem &st, uint8_t n) {
  const auto push_bit =
    [](auto &n, auto bit) {
      n *= 2U;
      n += bit;
    };
  do {
    st.cur = 0U;
    st.max = 0U;
    while (st.max < n - 1) {
      drawrnd(fd, st);
      push_bit(st.cur, st.dbit);
      st.dbit = 0U;
      push_bit(st.max, 1U);
    }
  } while (st.cur >= n);
  st.max = 0U;
}

static auto drawchar
(const file &fd, mem &st, const std::vector<char> &charset) {
  drawany(fd, st, charset.size());
  st.cur = charset[st.cur];
  s_write(1, st.cur);
  st.cur = 0U;
}

static auto drawpwd
(const file &fd,
 mem &st,
 int length,
 const std::vector<char> &charset) {
  for (auto i = 0; i < length; i++) {
    drawchar(fd, st, charset);
  }
  st.cur = '\n';
  s_write(1, st.cur);
}

void draw
(const char *dev, int length, const std::vector<char> &charset) {
  mem st;
  const auto fd = openrnd(st, dev);
  drawpwd(fd, st, length, charset);
}
