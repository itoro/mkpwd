INTRODUCTION
------------
mkpwd is a password generator written in C++. You can specify password
by setting the complexity bits count or charaters length. Charset used
for password can be set too.

USAGE
-----
Generate a password with default config
$ mkpwd
Generate a password with 8 alpha chars
$ mkpwd charset=a length=8
Generate a password with 64 entropy bits (a long int) with hexa digits
$ mkpwd charset=x entropy=64
Read the man mage
$ man mkpwd

BUILD AND INSTALL
-----------------
1) configuration
$ ./configure
or to set a prefix
$ ./configure --prefix="$HOME/local"
2) build
$ make
or to build with 4 processes
$ make -j4
3) install
$ make install
or to install in a temporary for packaging
$ make install DESTIR=/tmp/mkpwd-pkgdir
